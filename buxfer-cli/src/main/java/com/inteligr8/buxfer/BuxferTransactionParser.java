package com.inteligr8.buxfer;

import com.inteligr8.buxfer.model.Transaction;

public interface BuxferTransactionParser {
	
	ParsedTransaction parse(Transaction buxferTx) throws InterruptedException;

}
