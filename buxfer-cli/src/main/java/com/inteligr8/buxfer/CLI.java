package com.inteligr8.buxfer;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class CLI {
	
	public static void main(String[] args) throws ParseException, IOException, InterruptedException {
		Options options = new Options();
		if (args.length == 0) {
			help(options, null, null, "A function is required");
			return;
		} else if (args[0].equals("help")) {
			help(options, null, null);
			return;
		}
		
		String func = args[0];
		String[] subargs = Arrays.copyOfRange(args, 1, args.length);
		
		switch (func.toLowerCase()) {
			case "investdetail":
				InvestNormalizeCLI.main(subargs);
				break;
			case "investpl":
				InvestGainsLossesCLI.main(subargs);
				break;
			case "writer":
				WriterCLI.main(subargs);
				break;
			default:
				help(options, null, null, "The function '" + func + "' is not valid");
		}
	}
	
	static void help(Options options, String extraCommand, List<String> extraLines, String message) {
		System.err.println(message);
		help(options, extraCommand, extraLines);
	}
	
	static void help(Options options, String extraCommand, List<String> extraLines) {
		System.out.println("A set of Buxfer tools");
		System.out.print("usage: java -jar buxfer-cli-*.jar");
		System.out.print(" ( InvestDetail | InvestPL | Writer )");
		if (extraCommand != null)
			System.out.print(" " + extraCommand);
		System.out.println();
		HelpFormatter help = new HelpFormatter();
		help.printOptions(new PrintWriter(System.out, true), 120, options, 3, 3);
		System.out.println("InvestDetail: Reformats descriptions and sets the appropriate transaction types for investments");
		System.out.println("    InvestPL: After 'InvestDetail', compute profit/loss as transactions and reconciles where appropriate");
		System.out.println("      Writer: Writes transactions to external file");
		if (extraLines != null)
			for (String extraLine : extraLines)
				System.out.println(extraLine);
	}

}
