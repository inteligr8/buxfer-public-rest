package com.inteligr8.buxfer;

import java.io.IOException;
import java.io.OutputStream;

import com.inteligr8.buxfer.model.Transaction;

public class CsvTransactionWriter implements Observer<Transaction> {
	
	private final OutputStream ostream;
	
	public CsvTransactionWriter(OutputStream ostream) {
		this.ostream = ostream;
	}
	
	@Override
	public int observed(Transaction tx) throws IOException {
		return 0;
	}

}
