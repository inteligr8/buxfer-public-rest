package com.inteligr8.buxfer;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.buxfer.api.CommandApi;
import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Status;
import com.inteligr8.buxfer.model.Transaction.Type;
import com.inteligr8.buxfer.model.TransactionsResponse;

public class InvestGainsLossesCLI {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestGainsLossesCLI.class);

	private static final Pattern splitFormat =
			Pattern.compile("Split ([A-Z]+) ([0-9]+) to ([0-9]+)");
	
	private static final Map<String, Double> unsoldShares = new HashMap<>();
	private static final Map<String, List<TransactionWrapper>> unsoldBuys = new HashMap<>();
	
	public static void main(String[] args) throws ParseException, IOException, InterruptedException {
		Options options = buildOptions();
		String extraCommand = "[options] \"accountName\"";
		List<String> extraLines = Arrays.asList("accountName: A Buxfer account name");
		
		CommandLine cli = new DefaultParser().parse(options, args);
		if (cli.getArgList().isEmpty()) {
			CLI.help(options, extraCommand, extraLines, "An account name is required");
			return;
		} else if (cli.hasOption("help")) {
			CLI.help(options, extraCommand, extraLines);
			return;
		}
		
		final boolean taxExempt = cli.hasOption("tax-exempt");
		final boolean dryRun = cli.hasOption("dry-run");
		final int searchLimit = Integer.valueOf(cli.getOptionValue("limit", "0"));

		String buxferAccountName = cli.getArgList().iterator().next();
		final CommandApi bapi = findBuxferApi(cli);

		Observer<Transaction> observer = new Observer<Transaction>() {
			@Override
			public int observed(Transaction tx) throws IOException, InterruptedException {
				logger.debug("observed tx: {}", tx.getId());
				
				try {
					ParsedTransaction ptx = NormalizedParser.getInstance().parse(tx);
					if (!ptx.getType().equals(tx.getType()))
						throw new IllegalStateException("The tx " + tx.getId() + " description and its type conflict: one 'Buy' one 'Sell'");
					
					TransactionWrapper txw = new TransactionWrapper(ptx);
					switch (tx.getType()) {
						case Buy:
							registerBuy(txw);
							break;
						case Sell:
							return processSell(txw, bapi, taxExempt, dryRun);
						default:
							logger.debug("ignoring: " + tx.getDescription());
					}
				} catch (IllegalArgumentException iae) {
					Matcher matcher = splitFormat.matcher(tx.getDescription());
					if (matcher.matches()) {
						String symbol = matcher.group(1);
						int fromSecurities = Integer.valueOf(matcher.group(2));
						int toSecurities = Integer.valueOf(matcher.group(3));
						float ratio = 1f * toSecurities / fromSecurities;

						logger.debug("splitting {} with ratio {}", symbol, ratio);
						
						Double outstandingSecurities = unsoldShares.get(symbol);
						if (outstandingSecurities != null)
							unsoldShares.put(symbol, outstandingSecurities * ratio);
						
						if (unsoldBuys.get(symbol) != null)
							for (TransactionWrapper txw : unsoldBuys.get(symbol))
								txw.recordSplit(ratio);
					} else {
						logger.debug("ignoring: " + tx.getDescription());
					}
				}
				
				return 0;
			}
		};
	
		searchTransactions(bapi, buxferAccountName, searchLimit, observer);
		
		for (List<TransactionWrapper> txws : unsoldBuys.values()) {
			for (TransactionWrapper txw : txws) {
				if (txw.getParsedTransaction().getExpirationDate() != null) {
					if (LocalDate.now().isAfter(txw.getParsedTransaction().getExpirationDate())) {
						recordCapitalGain(txw.getOutstandingSecurities(), 0.0, txw.getParsedTransaction().getExpirationDate(), txw, bapi, taxExempt, dryRun);
						
						if (!dryRun)
							bapi.editTransaction(txw.getTransaction().getId(), txw.getParsedTransaction().getNormalizedDescription(), null, Status.Reconciled);
					}
				} else {
					long days = LocalDate.now().toEpochDay() - txw.getTransaction().getDate().toEpochDay();
					logger.debug("unreconciled tx {}: {} ({} days)", txw.getTransaction().getId(), txw.getTransaction().getDescription(), days);
					
					String description = txw.getDescription();
					if (!description.equals(txw.getTransaction().getDescription())) {
						logger.debug("updating partially reconciled tx {}: {}", txw.getTransaction().getId(), description);
						if (!dryRun)
							bapi.editTransaction(txw.getTransaction().getId(), description, null, null);
					}
				}
			}
		}
	}
		
	private static void registerBuy(TransactionWrapper txw) {
		Double unsoldSecurities = unsoldShares.get(txw.getParsedTransaction().getKey());
		unsoldSecurities = unsoldSecurities == null ? txw.getSplitAdjustedSecurities() : (unsoldSecurities + txw.getSplitAdjustedSecurities());
		unsoldShares.put(txw.getParsedTransaction().getKey(), unsoldSecurities);
		if (logger.isDebugEnabled())
			logger.debug("{} shares of: {}", unsoldShares.get(txw.getParsedTransaction().getKey()), txw.getParsedTransaction().getKey());
		
		List<TransactionWrapper> txws = unsoldBuys.get(txw.getParsedTransaction().getKey());
		if (txws == null)
			unsoldBuys.put(txw.getParsedTransaction().getKey(), txws = new LinkedList<TransactionWrapper>());
		logger.debug("{} outstanding buys of: {}", txws.size()+1, txw.getParsedTransaction().getKey());
		txws.add(txw);
	}
	
	private static int processSell(TransactionWrapper txw, CommandApi api, boolean taxExempt, boolean dryRun) {
		Double outstandingSecurities = unsoldShares.get(txw.getParsedTransaction().getKey());
		if (outstandingSecurities != null && outstandingSecurities < 0) {
			logger.debug("ignoring: {}", txw.getParsedTransaction().getKey());
			// security processing ignored
			return 0;
		} else if (outstandingSecurities == null || outstandingSecurities < txw.getSplitAdjustedSecurities()) {
			logger.warn("'" + txw.getParsedTransaction().getKey() + "' is out of shares; data integrity issue; not processing anymore sells on that security");
			unsoldShares.put(txw.getParsedTransaction().getKey(), Double.MIN_VALUE);
			return 0;
		}
		
		unsoldShares.put(txw.getParsedTransaction().getKey(), outstandingSecurities.doubleValue() - txw.getSplitAdjustedSecurities());
		if (logger.isDebugEnabled())
			logger.debug("{} shares of: {}", unsoldShares.get(txw.getParsedTransaction().getKey()), txw.getParsedTransaction().getKey());
		
		int reconciled = 0;
		String symbolTag = "Investment / Security / " + txw.getParsedTransaction().getSecuritySymbol();
		
		List<TransactionWrapper> buyTxws = unsoldBuys.get(txw.getParsedTransaction().getKey());
		ListIterator<TransactionWrapper> i = buyTxws.listIterator();
		//ListIterator<TransactionWrapper> i = buyTxws.listIterator(buyTxws.size());
		while (i.hasNext() && txw.getOutstandingSecurities() > 0) {
			TransactionWrapper buyTxw = i.next();
			logger.debug("reconciling with buy txId: {}", buyTxw.getTransaction().getId());

			double sharesToDrain = txw.getOutstandingSecurities();
			if (buyTxw.getOutstandingSecurities() <= sharesToDrain) {
				sharesToDrain = buyTxw.getOutstandingSecurities();
				i.remove();
			}
			
			double sellValueDrained = txw.decrementOutstandingSecurities(sharesToDrain);
			
			recordCapitalGain(sharesToDrain, sellValueDrained, txw.getTransaction().getDate(), buyTxw, api, taxExempt, dryRun);
		}
		
		if (!dryRun) {
			logger.debug("reconciled sell txId: {}", txw.getTransaction().getId());
			api.editTransaction(txw.getTransaction().getId(), null, symbolTag, Status.Reconciled);
			reconciled++;
		}
		
		return reconciled;
	}
	
	private static boolean recordCapitalGain(double sharesToDrain, double sellValueDrained, LocalDate sellDate, TransactionWrapper txw, CommandApi api, boolean taxExempt, boolean dryRun) {
		logger.debug("reconciling {} securities", sharesToDrain);
		
		double buyValueDrained = txw.decrementOutstandingSecurities(sharesToDrain);
		double gain = Math.round((sellValueDrained - buyValueDrained) * 100) / 100.0;
		
		long days = sellDate.toEpochDay() - txw.getTransaction().getDate().toEpochDay();
		boolean longTerm = sellDate.minusYears(1L).plusDays(1).isAfter(txw.getTransaction().getDate());
		
		String description = days + " Day " + (gain > 0.0 ? "Gain" : "Loss") + " " + txw.getParsedTransaction().getKey();

		String capitalGainTag = "Investment / Capital Gain / ";
		if (taxExempt) capitalGainTag += "Exempt";
		else if (longTerm) capitalGainTag += "Long Term";
		else capitalGainTag += "Short Term";
		String symbolTag = "Investment / Security / " + txw.getParsedTransaction().getSecuritySymbol();
		
		logger.debug("adding capital gain {}: {}", gain, description);
		
		if (!dryRun) {
			Long fromAccountId = (gain < 0.0) ? txw.getTransaction().getAccountId() : null;
			Long toAccountId = (gain < 0.0) ? null : txw.getTransaction().getAccountId();
			
			api.addTransaction(
					Type.Transfer.getOutgoingValue(),
					fromAccountId,
					toAccountId,
					sellDate,
					description,
					gain,
					symbolTag + "," + capitalGainTag,
					Status.Reconciled);
			
			if (txw.getOutstandingSecurities() == 0) {
				logger.debug("reconciled buy txId: {}", txw.getTransaction().getId());
				api.editTransaction(txw.getTransaction().getId(), txw.getDescription(), null, Status.Reconciled);
				return true;
			}
		}
		
		return false;
	}
	
	private static void searchTransactions(CommandApi api, String buxferAccountName, int limit, Observer<Transaction> observer) throws IOException, InterruptedException {
		TransactionsResponse response = api.getTransactionsInAccount(buxferAccountName, null, null, Status.Cleared, 1).getResponse();
		
		long total = response.getTotalItems();
		long maxToProcess = limit == 0 ? Long.MAX_VALUE : (long)limit;
		int pages = (int)((total-1) / 100) + 1;
		long processed = 0L;
		long reconciled = 0L;
		
		// must go in reverse
		for (int p = pages; p > 0 && processed < maxToProcess; p--) {
			logger.debug("searching '{}' page {}", buxferAccountName, p);
			response = api.getTransactionsInAccount(buxferAccountName, null, null, Status.Cleared, p).getResponse();
			if ((total-reconciled) != response.getTotalItems()) {
				logger.error("Transaction count changed unexpectedly while processing: {} => {}", total-reconciled, response.getTotalItems());
				return;
			}
			
			Collections.sort(response.getItems(), new Comparator<Transaction>() {
				@Override
				public int compare(Transaction tx1, Transaction tx2) {
					int compare = tx1.getDate().compareTo(tx2.getDate());
					if (compare == 0)
						// we want buys before sells
						compare = tx1.getDescription().compareTo(tx2.getDescription());
					return compare;
				}
			});
			
			for (Transaction tx : response.getItems()) {
				try {
					reconciled += observer.observed(tx);
				} catch (IllegalArgumentException | IllegalStateException ie) {
					logger.debug("transaction cannot be processed: " + ie.getMessage());
				}

				processed++;
				if (processed >= maxToProcess)
					break;
			}

			logger.info("Processed page {} ({} txs; {} existing tx reconciled)", p, processed, reconciled);

			// let the Buxfer indexer catch up
			Thread.sleep(5000L);
		}

		logger.info("Reconciled {} existing transactions ({} txs)", reconciled, processed);
	}
	
	private static Options buildOptions() {
		return new Options()
				.addOption(new Option("bu", "buxfer-email", true, "A Buxfer email for authentication"))
				.addOption(new Option("bp", "buxfer-password", true, "A Buxfer password for authentication"))
				.addOption(new Option("t", "tax-exempt", false, "Tag capital gains/losses and dividends as tax exempt"))
				.addOption(new Option("dr", "dry-run", false, "Do not updated Buxfer (for testing)"))
				.addOption(new Option("l", "limit", true, "Only process this many transactions (for testing)"));
	}
	
	private static CommandApi findBuxferApi(CommandLine cli) {
		BuxferClientConfiguration config = new BuxferClientConfiguration();
		config.setBaseUrl("https://www.buxfer.com");

		if (cli.hasOption("buxfer-email"))
			config.setAuthEmail(cli.getOptionValue("buxfer-email"));
		if (cli.hasOption("buxfer-password"))
			config.setAuthPassword(cli.getOptionValue("buxfer-password"));

		BuxferClientJerseyImpl client = new BuxferClientJerseyImpl(config);
		return client.getApi().getCommandApi();
	}

}
