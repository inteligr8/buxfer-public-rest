package com.inteligr8.buxfer;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Status;
import com.inteligr8.buxfer.model.TransactionsResponse;
import com.inteligr8.polygon.PolygonClientConfiguration;
import com.inteligr8.polygon.PolygonClientJerseyImpl;
import com.inteligr8.polygon.PolygonPublicRestApi;

public class InvestNormalizeCLI {
	
	private static final Logger logger = LoggerFactory.getLogger(InvestNormalizeCLI.class);
	
	public static void main(String[] args) throws ParseException, IOException, InterruptedException {
		Options options = buildOptions();
		String extraCommand = "[options] \"accountName\"";
		List<String> extraLines = Arrays.asList("accountName: A Buxfer account name");
		
		CommandLine cli = new DefaultParser().parse(options, args);
		if (cli.getArgList().isEmpty()) {
			CLI.help(options, extraCommand, extraLines, "An account name is required");
			return;
		} else if (cli.hasOption("help")) {
			CLI.help(options, extraCommand, extraLines);
			return;
		}
		
		final boolean dryRun = cli.hasOption("dry-run");
		final int searchLimit = Integer.valueOf(cli.getOptionValue("limit", "0"));

		String buxferAccountName = cli.getArgList().iterator().next();
		final BuxferPublicRestApi bapi = findBuxferApi(cli);
		final PolygonPublicRestApi papi = findPolygonApi(cli);
		
		final List<BuxferTransactionParser> parsers = Arrays.asList(
				TdAmeritradeParser.getInstance(),
				SofiInvestParser.getInstance(papi));

		Observer<Transaction> observer = new Observer<Transaction>() {
			@Override
			public int observed(Transaction tx) throws IOException, InterruptedException {
				logger.debug("observed tx: {}", tx.getId());
				
				// ignore null/empty descriptions
				if (tx.getDescription() == null || tx.getDescription().length() == 0)
					return 0;
				
				try {
					NormalizedParser.getInstance().parse(tx);
					logger.trace("tx {} already formatted/processed: {}", tx.getId(), tx.getDescription());
					return 0;
				} catch (IllegalArgumentException iae) {
					// continue
				}
				
				for (BuxferTransactionParser parser : parsers) {
					try {
						ParsedTransaction ptx = parser.parse(tx);
						logger.debug("tx {} formatted: {} => {}", tx.getId(), tx.getDescription(), ptx.getNormalizedDescription());
						
						if (!tx.getType().equals(ptx.getType()))
							logger.debug("tx {} re-typed: {} => {}", tx.getId(), tx.getType(), ptx.getType());
						
						if (!dryRun)
							bapi.getCommandApi().editTransaction(tx.getId(), ptx.getType().getOutgoingValue(), ptx.getNormalizedDescription(),
									"Investment / Security / " + ptx.getSecuritySymbol(), null);
						return 1;
					} catch (IllegalArgumentException iae) {
						logger.trace("failed to parse");
						// try another
					}
				}

				logger.info("tx {} cannot be formatted: {}", tx.getId(), tx.getDescription());
				return 0;
			}
		};
	
		searchTransactions(bapi, buxferAccountName, searchLimit, observer);
	}
	
	private static void searchTransactions(BuxferPublicRestApi api, String buxferAccountName, int limit, Observer<Transaction> observer) throws IOException, InterruptedException {
		long maxToProcess = limit == 0 ? Long.MAX_VALUE : (long)limit;
		long total = -1L;
		long processed = 0L;
		long updated = 0L;
		int pages = 1;
		for (int p = 1; p <= pages && processed < maxToProcess; p++) {
			logger.debug("searching '{}' page {}", buxferAccountName, p);
			TransactionsResponse response = api.getCommandApi().getTransactionsInAccount(buxferAccountName, null, null, Status.Cleared, p).getResponse();
			if (total < 0L) {
				total = response.getTotalItems();
				pages = (int)((response.getTotalItems()-1) / 100) + 1;
				logger.debug("found {} total transactions covering {} pages", total, pages);
			} else if (total != response.getTotalItems()) {
				logger.error("Transaction count changed while processing");
				return;
			}
			
			for (Transaction tx : response.getItems()) {
				updated += observer.observed(tx);
				processed++;
				if (processed >= maxToProcess)
					break;
			}

			logger.info("Processed page {} ({} txs; {} updated)", p, processed, updated);
		}

		logger.info("Updated {} transactions ({} txs)", updated, processed);
	}
	
	private static Options buildOptions() {
		return new Options()
				.addOption(new Option("bu", "buxfer-email", true, "A Buxfer email for authentication"))
				.addOption(new Option("bp", "buxfer-password", true, "A Buxfer password for authentication"))
				.addOption(new Option("pak", "polygon-apiKey", true, "A Polygon.IO API key for authentication"))
				.addOption(new Option("dr", "dry-run", false, "Do not updated Buxfer (for testing)"))
				.addOption(new Option("l", "limit", true, "Only process this many transactions (for testing)"));
	}
	
	private static BuxferPublicRestApi findBuxferApi(CommandLine cli) {
		BuxferClientConfiguration config = new BuxferClientConfiguration();
		config.setBaseUrl("https://www.buxfer.com");

		if (cli.hasOption("buxfer-email"))
			config.setAuthEmail(cli.getOptionValue("buxfer-email"));
		if (cli.hasOption("buxfer-password"))
			config.setAuthPassword(cli.getOptionValue("buxfer-password"));

		BuxferClientJerseyImpl client = new BuxferClientJerseyImpl(config);
		return client.getApi();
	}
	
	private static PolygonPublicRestApi findPolygonApi(CommandLine cli) {
		PolygonClientConfiguration config = new PolygonClientConfiguration();
		config.setBaseUrl("https://api.polygon.io");

		if (cli.hasOption("polygon-apiKey"))
			config.setApiKey(cli.getOptionValue("polygon-apiKey"));

		PolygonClientJerseyImpl client = new PolygonClientJerseyImpl(config);
		return client.getApi();
	}

}
