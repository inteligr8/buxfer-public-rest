package com.inteligr8.buxfer;

import java.io.IOException;
import java.io.OutputStream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inteligr8.buxfer.model.Transaction;

public class JsonTransactionWriter implements Observer<Transaction> {
	
	private final ObjectMapper om = new ObjectMapper();
	private final OutputStream ostream;
	
	public JsonTransactionWriter(OutputStream ostream) {
		this.ostream = ostream;
	}
	
	@Override
	public int observed(Transaction tx) throws IOException {
		this.om.writeValue(this.ostream, tx);
		return 1;
	}

}
