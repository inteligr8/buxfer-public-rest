package com.inteligr8.buxfer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Type;

public class NormalizedDividendTransaction implements ParsedTransaction {

	private final Pattern descriptionFormat =
			Pattern.compile("((Qualified|Ordinary) )?Dividend ([A-Z]+)");
	
	private final Transaction tx;
	private final String symbol;
	
	public NormalizedDividendTransaction(Transaction tx) {
		this.tx = tx;
		
		Matcher matcher = this.descriptionFormat.matcher(tx.getDescription());
		if (!matcher.find())
			throw new IllegalArgumentException();
		
		this.symbol = matcher.group(3).toUpperCase();
	}
	
	@Override
	public Transaction getTransaction() {
		return this.tx;
	}
	
	@Override
	public String getNormalizedDescription() {
		return this.tx.getDescription();
	}
	
	@Override
	public Type getType() {
		return Type.Dividend;
	}
	
	@Override
	public String getKey() {
		return this.symbol;
	}
	
	@Override
	public double getSecurities() {
		return Double.NaN;
	}
	
	@Override
	public String getSecuritySymbol() {
		return this.symbol;
	}
	
	@Override
	public double getPricePerSecurity() {
		return Double.NaN;
	}

}
