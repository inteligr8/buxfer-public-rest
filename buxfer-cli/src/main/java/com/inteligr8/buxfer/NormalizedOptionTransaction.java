package com.inteligr8.buxfer;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Type;

public class NormalizedOptionTransaction implements ParsedTransaction, PartiallyReconciledTransaction {

	private final Pattern descriptionFormat =
			Pattern.compile("(Bought|Sold) ([0-9\\.]+) ([A-Z]+) (2[0-9]{3}-[0-9]{2}-[0-9]{2}) ([0-9\\.]+) (PUT|CALL) @ ([0-9\\.]+)(; ([0-9\\.]+) unsold)?");
	
	private final Transaction tx;
	private final Type type;
	private final double securities;
	private final Double unsoldSecurities;
	private final String symbol;
	private final LocalDate expirationDate;
	private final double strikePrice;
	private final ContractType contractType;
	private final double perSecurityPrice;
	private final String description;
	
	public NormalizedOptionTransaction(Transaction tx) {
		this.tx = tx;
		
		Matcher matcher = this.descriptionFormat.matcher(tx.getDescription());
		if (!matcher.find())
			throw new IllegalArgumentException();
		
		String buysell = matcher.group(1);
		this.securities = Double.valueOf(matcher.group(2));
		this.symbol = matcher.group(3).toUpperCase();
		this.expirationDate = LocalDate.parse(matcher.group(4));
		this.strikePrice = Double.valueOf(matcher.group(5));
		this.contractType = ContractType.valueOf(matcher.group(6));
		this.perSecurityPrice = Double.valueOf(matcher.group(7));
		String unsoldSecurities = matcher.group(9);
		
		this.type = this.determineTransactionType(buysell);
		this.unsoldSecurities = unsoldSecurities == null ? null : Double.valueOf(unsoldSecurities);

		this.description = new StringBuilder()
				.append(buysell).append(' ')
				.append(NumberFormatFactory.getSecuritiesFormatter().format(this.securities)).append(' ')
				.append(this.symbol).append(' ')
				.append(this.expirationDate.toString()).append(' ')
				.append(NumberFormatFactory.getStrikePriceFormatter().format(this.strikePrice)).append(' ')
				.append(this.contractType.toString()).append(" @ ")
				.append(NumberFormatFactory.getPriceFormatter().format(this.perSecurityPrice)).toString();
	}
	
	private Type determineTransactionType(String buysell) {
		buysell = buysell.toLowerCase();
		if (buysell.charAt(0) == 'b') {
			return Type.Buy;
		} else if (buysell.charAt(0) == 's') {
			return Type.Sell;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public Transaction getTransaction() {
		return this.tx;
	}
	
	@Override
	public String getNormalizedDescription() {
		return this.description;
	}
	
	@Override
	public Type getType() {
		return this.type;
	}
	
	@Override
	public String getKey() {
		return this.symbol + ":" + this.expirationDate + ":" + this.strikePrice + ":" + this.contractType;
	}
	
	@Override
	public double getSecurities() {
		return this.securities;
	}
	
	@Override
	public Double getUnsoldSecurities() {
		return this.unsoldSecurities;
	}
	
	@Override
	public String getSecuritySymbol() {
		return this.symbol;
	}
	
	@Override
	public double getPricePerSecurity() {
		return this.perSecurityPrice;
	}
	
	@Override
	public ContractType getContractType() {
		return this.contractType;
	}
	
	@Override
	public LocalDate getExpirationDate() {
		return this.expirationDate;
	}
	
	@Override
	public Double getStrikePrice() {
		return this.strikePrice;
	}

}
