package com.inteligr8.buxfer;

import com.inteligr8.buxfer.model.Transaction;

public class NormalizedParser implements BuxferTransactionParser {
	
	private static final NormalizedParser INSTANCE = new NormalizedParser();
	
	public static NormalizedParser getInstance() {
		return INSTANCE;
	}
	
	
	
	private NormalizedParser() {
	}
	
	@Override
	public ParsedTransaction parse(Transaction buxferTx) {
		try {
			return new NormalizedOptionTransaction(buxferTx);
		} catch (IllegalArgumentException iae) {
			try {
				return new NormalizedStockTransaction(buxferTx);
			} catch (IllegalArgumentException iae2) {
				return new NormalizedDividendTransaction(buxferTx);
			}
		}
	}

}
