package com.inteligr8.buxfer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Type;

public class NormalizedStockTransaction implements ParsedTransaction, PartiallyReconciledTransaction {

	private final Pattern descriptionFormat =
			Pattern.compile("(Bought|Sold) ([0-9\\.]+) ([A-Z]+) @ ([0-9\\.]+)(; ([0-9\\.]+) unsold)?");
	
	private final Transaction tx;
	private final Type type;
	private final double securities;
	private final Double unsoldSecurities;
	private final String symbol;
	private final String description;
	private final double perSecurityPrice;
	
	public NormalizedStockTransaction(Transaction tx) {
		this.tx = tx;
		
		Matcher matcher = this.descriptionFormat.matcher(tx.getDescription());
		if (!matcher.find())
			throw new IllegalArgumentException();
		
		String buysell = matcher.group(1);
		this.securities = Double.valueOf(matcher.group(2));
		this.symbol = matcher.group(3).toUpperCase();
		this.perSecurityPrice = Double.valueOf(matcher.group(4));
		String unsoldSecurities = matcher.group(6);
		
		this.type = this.determineTransactionType(buysell);
		this.unsoldSecurities = unsoldSecurities == null ? null : Double.valueOf(unsoldSecurities);
		
		this.description = new StringBuilder()
				.append(buysell).append(' ')
				.append(NumberFormatFactory.getSecuritiesFormatter().format(this.securities)).append(' ')
				.append(this.symbol).append(" @ ")
				.append(NumberFormatFactory.getPriceFormatter().format(this.perSecurityPrice)).toString();
	}
	
	private Type determineTransactionType(String buysell) {
		buysell = buysell.toLowerCase();
		if (buysell.charAt(0) == 'b') {
			return Type.Buy;
		} else if (buysell.charAt(0) == 's') {
			return Type.Sell;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public Transaction getTransaction() {
		return this.tx;
	}
	
	@Override
	public String getNormalizedDescription() {
		return this.description;
	}
	
	@Override
	public Type getType() {
		return this.type;
	}
	
	@Override
	public String getKey() {
		return this.symbol;
	}
	
	@Override
	public double getSecurities() {
		return this.securities;
	}
	
	@Override
	public Double getUnsoldSecurities() {
		return this.unsoldSecurities;
	}
	
	@Override
	public String getSecuritySymbol() {
		return this.symbol;
	}
	
	@Override
	public double getPricePerSecurity() {
		return this.perSecurityPrice;
	}

}
