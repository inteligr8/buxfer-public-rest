package com.inteligr8.buxfer;

import java.math.RoundingMode;
import java.text.NumberFormat;

public class NumberFormatFactory {
	
	private static final NumberFormat strikePriceFormatter = NumberFormat.getNumberInstance();
	static {
		strikePriceFormatter.setMaximumFractionDigits(4);
		strikePriceFormatter.setMinimumFractionDigits(1);
		strikePriceFormatter.setRoundingMode(RoundingMode.HALF_UP);
		strikePriceFormatter.setGroupingUsed(false);
	}
	
	private static final NumberFormat priceFormatter = NumberFormat.getNumberInstance();
	static {
		priceFormatter.setMaximumFractionDigits(4);
		priceFormatter.setMinimumFractionDigits(0);
		priceFormatter.setRoundingMode(RoundingMode.HALF_UP);
		priceFormatter.setGroupingUsed(false);
	}
	
	private static final NumberFormat securityFormatter = NumberFormat.getNumberInstance();
	static {
		securityFormatter.setMaximumFractionDigits(12);
		securityFormatter.setMinimumFractionDigits(0);
		securityFormatter.setRoundingMode(RoundingMode.HALF_UP);
		securityFormatter.setGroupingUsed(false);
	}
	
	public static NumberFormat getStrikePriceFormatter() {
		return strikePriceFormatter;
	}
	
	public static NumberFormat getPriceFormatter() {
		return priceFormatter;
	}
	
	public static NumberFormat getSecuritiesFormatter() {
		return securityFormatter;
	}

}
