package com.inteligr8.buxfer;

public class NumericRange<T extends Number> {
	
	private final T from;
	private final T to;
	
	public NumericRange(T fromAndTo) {
		this.from = fromAndTo;
		this.to = fromAndTo;
	}
	
	public NumericRange(T from, T to) {
		this.from = from;
		this.to = to;
	}
	
	public T getFrom() {
		return this.from;
	}
	
	public T getTo() {
		return this.to;
	}
	
	public boolean isPoint() {
		return this.from.equals(this.to);
	}
	
	public double getMidpoint() {
		return (this.from.doubleValue() + this.to.doubleValue()) / 2.0;
	}
	
	@Override
	public String toString() {
		return this.from + "-" + this.to;
	}

}
