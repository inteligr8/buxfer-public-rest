package com.inteligr8.buxfer;

import java.io.IOException;

public interface Observer<T> {
	
	int observed(T t) throws IOException, InterruptedException;

}
