package com.inteligr8.buxfer;

import java.time.LocalDate;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Type;

public interface ParsedTransaction {
	
	public enum ContractType {
		PUT,
		CALL
	}
	
	/**
	 * This provides the Buxfer transaction object and all of its raw meta-data.
	 */
	Transaction getTransaction();
	
	String getNormalizedDescription();
	
	/**
	 * This provides a hash key for matching buys with sells.  It should uniquely identify the security.
	 */
	String getKey();
	
	/**
	 * This provides the type of transaction as described in the transaction's description.
	 */
	Type getType();
	
	/**
	 * This provides the number of shares, option contracts, or even fractional shares (for crypto) in the transaction.
	 */
	double getSecurities();
	
	/**
	 * This provides only the security symbol of the transaction; not the expiration date or strike price included (for options).
	 */
	String getSecuritySymbol();
	
	double getPricePerSecurity();
	
	/**
	 * This provides the option contract type: Put or Call
	 * 
	 * @return The option contract type; null if not an option
	 */
	default ContractType getContractType() {
		return null;
	}
	
	default LocalDate getExpirationDate() {
		return null;
	}
	
	default Double getStrikePrice() {
		return null;
	}

}
