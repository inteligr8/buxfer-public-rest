package com.inteligr8.buxfer;

public interface PartiallyReconciledTransaction {
	
	Double getUnsoldSecurities();

}
