package com.inteligr8.buxfer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.polygon.PolygonPublicRestApi;

public class SofiInvestParser implements BuxferTransactionParser {
	
	private static SofiInvestParser INSTANCE;
	
	public synchronized static SofiInvestParser getInstance(PolygonPublicRestApi papi) {
		if (INSTANCE == null)
			INSTANCE = new SofiInvestParser(papi);
		return INSTANCE;
	}
	
	

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final PolygonPublicRestApi papi;
	
	private SofiInvestParser(PolygonPublicRestApi papi) {
		this.papi = papi;
	}
	
	@Override
	public ParsedTransaction parse(Transaction buxferTx) throws InterruptedException {
		try {
			return new SofiInvestTransaction(buxferTx, this.papi);
		} catch (IllegalArgumentException iae) {
			this.logger.trace("Not a SoFi tx: {}", buxferTx.getId());
			throw iae;
		}
	}

}
