package com.inteligr8.buxfer;

import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Type;
import com.inteligr8.polygon.PolygonPublicRestApi;
import com.inteligr8.polygon.model.StockDateSummary;

public class SofiInvestTransaction implements ParsedTransaction {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final Pattern descriptionFormat =
			Pattern.compile("(Buy|Sell|Dividend) ([A-Za-z]+)");

	private static long nextExecutionTime = 0L;
	private final long polygonServiceThrottleTime = 20L * 1000L;
	
	private final PolygonPublicRestApi papi;
	private final Transaction tx;
	private final Type type;
	private final NumericRange<? extends Number> securities;
	private final String symbol;
	private final String description;
	private final NumericRange<Double> perSecurityPrice;
	
	public SofiInvestTransaction(Transaction tx, PolygonPublicRestApi papi) throws InterruptedException {
		this.papi = papi;
		this.tx = tx;
		
		Matcher matcher = this.descriptionFormat.matcher(tx.getDescription());
		if (!matcher.find())
			throw new IllegalArgumentException();
		
		String buysell = matcher.group(1);
		this.symbol = matcher.group(2).toUpperCase();

		this.type = this.determineTransactionType(buysell);
		
		if (Type.Dividend.equals(this.type)) {
			this.description = "Dividend " + this.symbol;
			this.securities = null;
			this.perSecurityPrice = null;
		} else {
			this.throttle();
			
			this.securities = this.estimateSecurites();
			this.perSecurityPrice = this.estimatePrice(this.securities);
			
			String action = Type.Buy.equals(type) ? "Bought" : "Sold";
			this.description = new StringBuilder()
					.append(action).append(' ')
					.append(this.getRange(NumberFormatFactory.getSecuritiesFormatter(), this.securities)).append(' ')
					.append(this.symbol).append(" @ ")
					.append(this.getRange(NumberFormatFactory.getPriceFormatter(), this.perSecurityPrice)).toString();
		}
	}
	
	private Type determineTransactionType(String buysell) {
		buysell = buysell.toLowerCase();
		if (buysell.charAt(0) == 'b') {
			return Type.Buy;
		} else if (buysell.charAt(0) == 's') {
			return Type.Sell;
		} else if (buysell.charAt(0) == 'd') {
			return Type.Dividend;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	private synchronized void throttle() throws InterruptedException {
		// Polygon.IO throttling
		if (SofiInvestTransaction.nextExecutionTime > System.currentTimeMillis()) {
			long waitTime = SofiInvestTransaction.nextExecutionTime - System.currentTimeMillis();
			logger.debug("Throttled for Polygon.IO service for {} ms", waitTime);
			Thread.sleep(waitTime);
		}
		SofiInvestTransaction.nextExecutionTime = System.currentTimeMillis() + this.polygonServiceThrottleTime;
	}
	
	protected NumericRange<? extends Number> estimateSecurites() {
		this.logger.debug("Searching for price activity for {} on {}", symbol, this.tx.getDate());			
		StockDateSummary summary = this.papi.getStocksApi().getStockSummaryOnDate(this.symbol, this.tx.getDate(), false);
		if (summary.getLow() == null || summary.getHigh() == null)
			return null;
		
		double maxFracShares = this.tx.getAmount() / summary.getLow();
		double minFracShares = this.tx.getAmount() / summary.getHigh();
		int maxShares = (int)Math.floor(maxFracShares);
		int minShares = (int)Math.ceil(minFracShares);
		
		if (minShares == maxShares) {
			logger.debug("Found the highly likely shares: {}", minShares);
			return new NumericRange<Integer>(minShares);
		} else if (minShares < maxShares) {
			logger.info("Found a range of possible shares; needs manual update: {} => {}", this.tx.getDate(), this.tx.getDescription());
			return new NumericRange<Integer>(minShares, maxShares);
		} else if (maxFracShares - maxShares < 0.05) {
			logger.debug("Found the highly likely shares (w/ commission): {}", maxShares);
			return new NumericRange<Integer>(maxShares);
		} else {
			logger.info("Found a fractional range of possible shares; needs manual update: {} => {}", this.tx.getDate(), this.tx.getDescription());
			return new NumericRange<Double>(minFracShares, maxFracShares);
		}
	}
	
	protected NumericRange<Double> estimatePrice(NumericRange<? extends Number> securities) {
		if (securities == null)
			return null;
		if (securities.isPoint())
			return new NumericRange<Double>(Math.round(this.tx.getAmount().doubleValue() * 100 / securities.getFrom().doubleValue()) / 100.0);
		
		return new NumericRange<Double>(
				Math.round(this.tx.getAmount().doubleValue() * 100 / securities.getTo().doubleValue()) / 100.0,
				Math.round(this.tx.getAmount().doubleValue() * 100 / securities.getFrom().doubleValue()) / 100.0);
	}
	
	@Override
	public Transaction getTransaction() {
		return this.tx;
	}
	
	@Override
	public String getNormalizedDescription() {
		return this.description;
	}
	
	@Override
	public Type getType() {
		return this.type;
	}
	
	@Override
	public String getKey() {
		return this.symbol;
	}
	
	@Override
	public double getSecurities() {
		if (this.securities == null) return Double.NaN;
		else if (this.securities.isPoint()) return this.securities.getFrom().doubleValue();
		else return this.securities.getMidpoint();
	}
	
	@Override
	public String getSecuritySymbol() {
		return this.symbol;
	}
	
	@Override
	public double getPricePerSecurity() {
		if (this.perSecurityPrice == null) return Double.NaN;
		else if (this.perSecurityPrice.isPoint()) return this.perSecurityPrice.getFrom().doubleValue();
		else return this.perSecurityPrice.getMidpoint();
	}
	
	private String getRange(NumberFormat formatter, NumericRange<? extends Number> range) {
		if (range == null) {
			return "NaN";
		} else if (range.isPoint()) {
			return formatter.format(range.getFrom().doubleValue());
		} else {
			return formatter.format(range.getFrom().doubleValue()) + "-" + formatter.format(range.getTo().doubleValue());
		}
	}

}
