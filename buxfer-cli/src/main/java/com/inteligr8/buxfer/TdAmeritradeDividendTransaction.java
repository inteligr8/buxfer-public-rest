package com.inteligr8.buxfer;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Type;

public class TdAmeritradeDividendTransaction implements ParsedTransaction {

	private final List<Pattern> descriptionFormats = Arrays.asList(
			Pattern.compile("(Qualified|Ordinary) Dividend \\(([A-Za-z]+)\\)"),
			Pattern.compile("(Qualified|Ordinary) Dividend~([A-Za-z]+)"));
	
	private final Transaction tx;
	private final String symbol;
	private final String description;
	
	public TdAmeritradeDividendTransaction(Transaction tx) {
		this.tx = tx;
		
		Matcher matcher = null;
		for (Pattern descriptionFormat : this.descriptionFormats) {
			matcher = descriptionFormat.matcher(tx.getDescription());
			if (matcher.find())
				break;
			matcher = null;
		}
		
		if (matcher == null)
			throw new IllegalArgumentException();

		String dividendType = matcher.group(1);
		this.symbol = matcher.group(2).toUpperCase();
		
		this.description = new StringBuilder()
				.append(dividendType).append(" Dividend ")
				.append(this.symbol).toString();
	}
	
	@Override
	public Transaction getTransaction() {
		return this.tx;
	}
	
	@Override
	public String getNormalizedDescription() {
		return this.description;
	}
	
	@Override
	public Type getType() {
		return Type.Dividend;
	}
	
	@Override
	public String getKey() {
		return this.symbol;
	}
	
	@Override
	public double getSecurities() {
		return Double.NaN;
	}
	
	@Override
	public String getSecuritySymbol() {
		return this.symbol;
	}
	
	@Override
	public double getPricePerSecurity() {
		return Double.NaN;
	}

}
