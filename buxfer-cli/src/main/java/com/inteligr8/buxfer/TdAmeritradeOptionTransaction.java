package com.inteligr8.buxfer;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Type;

public class TdAmeritradeOptionTransaction implements ParsedTransaction {
	
	private final Pattern descriptionFormat =
			Pattern.compile(".*(Bought|Sold)( To (Open|Close))? ([0-9\\.]+) ([A-Za-z]+) ([A-Za-z]+) ([0-9Xx]+) (2[0-9]{3}) ([0-9\\.]+) (Put|Call) @ ([0-9\\.]+)");
	
	private final Transaction tx;
	private final Type type;
	private final int securities;
	private final String symbol;
	private final LocalDate expirationDate;
	private final double strikePrice;
	private final ContractType contractType;
	private final double perSecurityPrice;
	private final String description;
	
	public TdAmeritradeOptionTransaction(Transaction tx) {
		this.tx = tx;
		
		Matcher matcher = this.descriptionFormat.matcher(tx.getDescription());
		if (!matcher.find())
			throw new IllegalArgumentException();
		
		String buysell = matcher.group(1);
		//String openclose = matcher.group(3).toLowerCase();
		this.securities = Integer.valueOf(matcher.group(4));
		this.symbol = matcher.group(5).toUpperCase();
		String strikeMonth = matcher.group(6);
		String strikeDay = matcher.group(7);
		String strikeYear = matcher.group(8);
		this.strikePrice = Double.valueOf(matcher.group(9));
		this.contractType = ContractType.valueOf(matcher.group(10).toUpperCase());
		this.perSecurityPrice = Double.valueOf(matcher.group(11));
		
		this.type = this.determineTransactionType(buysell);
		this.expirationDate = this.determineExpirationDate(strikeYear, strikeMonth, strikeDay);
		
		String action = Type.Buy.equals(type) ? "Bought" : "Sold";
		this.description = new StringBuilder()
				.append(action).append(' ')
				.append(NumberFormatFactory.getSecuritiesFormatter().format(this.securities)).append(' ')
				.append(this.symbol).append(' ')
				.append(this.expirationDate.toString()).append(' ')
				.append(NumberFormatFactory.getStrikePriceFormatter().format(this.strikePrice)).append(' ')
				.append(this.contractType.toString()).append(" @ ")
				.append(NumberFormatFactory.getPriceFormatter().format(this.perSecurityPrice)).toString();
	}
	
	private Type determineTransactionType(String buysell) {
		buysell = buysell.toLowerCase();
		if (buysell.charAt(0) == 'b') {
			return Type.Buy;
		} else if (buysell.charAt(0) == 's') {
			return Type.Sell;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	private LocalDate determineExpirationDate(String strikeYearStr, String strikeMonthStr, String strikeDayStr) {
		return this.determineExpirationDate(
				Integer.parseInt(strikeYearStr),
				this.parseStrikeMonth(strikeMonthStr),
				strikeDayStr);
	}
	
	private LocalDate determineExpirationDate(int strikeYear, Month strikeMonth, String strikeDay) {
		try {
			return LocalDate.of(Integer.valueOf(strikeYear), strikeMonth, Integer.valueOf(strikeDay));
		} catch (NumberFormatException nfe) {
			LocalDate expirationDate = LocalDate.of(Integer.valueOf(strikeYear), strikeMonth, 1);
			while (!DayOfWeek.FRIDAY.equals(expirationDate.getDayOfWeek()))
				expirationDate = expirationDate.plusDays(1L);
			return expirationDate.plusWeeks(2L); // 3rd friday of month
		}
	}
	
	@Override
	public Transaction getTransaction() {
		return this.tx;
	}
	
	@Override
	public String getNormalizedDescription() {
		return this.description;
	}
	
	@Override
	public Type getType() {
		return this.type;
	}
	
	@Override
	public String getKey() {
		return this.symbol + ":" + this.expirationDate + ":" + this.strikePrice + ":" + this.contractType;
	}
	
	@Override
	public double getSecurities() {
		return (double)this.securities;
	}
	
	@Override
	public String getSecuritySymbol() {
		return this.symbol;
	}
	
	@Override
	public double getPricePerSecurity() {
		return this.perSecurityPrice;
	}
	
	@Override
	public ContractType getContractType() {
		return this.contractType;
	}
	
	@Override
	public LocalDate getExpirationDate() {
		return this.expirationDate;
	}
	
	@Override
	public Double getStrikePrice() {
		return this.strikePrice;
	}
	
	protected Month parseStrikeMonth(String month) {
		month = month.toLowerCase();
		switch (month.charAt(0)) {
			case 'f': return Month.FEBRUARY;
			case 's': return Month.SEPTEMBER;
			case 'o': return Month.OCTOBER;
			case 'n': return Month.NOVEMBER;
			case 'd': return Month.DECEMBER;
			case 'j':
				switch (month.substring(0, 3)) {
					case "jan": return Month.JANUARY;
					case "jun": return Month.JUNE;
					case "jul": return Month.JULY;
					default: return null;
				}
			case 'm':
				switch (month.charAt(2)) {
					case 'r': return Month.MARCH;
					case 'y': return Month.MAY;
					default: return null;
				}
			case 'a':
				switch (month.charAt(1)) {
					case 'p': return Month.APRIL;
					case 'u': return Month.AUGUST;
					default: return null;
				}
			default: return null;
		}
	}

}
