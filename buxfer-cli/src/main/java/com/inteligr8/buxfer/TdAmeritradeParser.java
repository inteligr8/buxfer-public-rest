package com.inteligr8.buxfer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.buxfer.model.Transaction;

public class TdAmeritradeParser implements BuxferTransactionParser {

	private static final TdAmeritradeParser INSTANCE = new TdAmeritradeParser();
	
	public static TdAmeritradeParser getInstance() {
		return INSTANCE;
	}
	

	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private TdAmeritradeParser() {
	}
	
	@Override
	public ParsedTransaction parse(Transaction buxferTx) {
		try {
			return new TdAmeritradeOptionTransaction(buxferTx);
		} catch (IllegalArgumentException iae) {
			this.logger.trace("Not a TD Ameritrade option tx: {}", buxferTx.getId());
			
			try {
				return new TdAmeritradeStockTransaction(buxferTx);
			} catch (IllegalArgumentException iae2) {
				this.logger.trace("Not a TD Ameritrade stock tx: {}", buxferTx.getId());
				
				try {
					return new TdAmeritradeDividendTransaction(buxferTx);
				} catch (IllegalArgumentException iae3) {
					this.logger.trace("Not a TD Ameritrade dividend tx: {}", buxferTx.getId());
					throw iae3;
				}
			}
		}
	}

}
