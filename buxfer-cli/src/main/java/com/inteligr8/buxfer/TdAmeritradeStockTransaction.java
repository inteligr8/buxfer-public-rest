package com.inteligr8.buxfer;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.Transaction.Type;

public class TdAmeritradeStockTransaction implements ParsedTransaction {

	private final Pattern descriptionFormat =
			Pattern.compile(".*(Bought|Sold) ([0-9\\.]+) ([A-Za-z]+) @ ([0-9\\.]+)");
	
	private final Transaction tx;
	private final Type type;
	private final int securities;
	private final String symbol;
	private final String description;
	private final double perSecurityPrice;
	
	public TdAmeritradeStockTransaction(Transaction tx) {
		this.tx = tx;
		
		Matcher matcher = this.descriptionFormat.matcher(tx.getDescription());
		if (!matcher.find())
			throw new IllegalArgumentException();
		
		String buysell = matcher.group(1);
		this.securities = Integer.valueOf(matcher.group(2));
		this.symbol = matcher.group(3).toUpperCase();
		this.perSecurityPrice = Double.valueOf(matcher.group(4));

		this.type = this.determineTransactionType(buysell);
		
		String action = Type.Buy.equals(type) ? "Bought" : "Sold";
		this.description = new StringBuilder()
				.append(action).append(' ')
				.append(NumberFormatFactory.getSecuritiesFormatter().format(this.securities)).append(' ')
				.append(this.symbol).append(" @ ")
				.append(NumberFormatFactory.getPriceFormatter().format(this.perSecurityPrice)).toString();
	}
	
	private Type determineTransactionType(String buysell) {
		buysell = buysell.toLowerCase();
		if (buysell.charAt(0) == 'b') {
			return Type.Buy;
		} else if (buysell.charAt(0) == 's') {
			return Type.Sell;
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public Transaction getTransaction() {
		return this.tx;
	}
	
	@Override
	public String getNormalizedDescription() {
		return this.description;
	}
	
	@Override
	public Type getType() {
		return this.type;
	}
	
	@Override
	public String getKey() {
		return this.symbol;
	}
	
	@Override
	public double getSecurities() {
		return (double)this.securities;
	}
	
	@Override
	public String getSecuritySymbol() {
		return this.symbol;
	}
	
	@Override
	public double getPricePerSecurity() {
		return this.perSecurityPrice;
	}

}
