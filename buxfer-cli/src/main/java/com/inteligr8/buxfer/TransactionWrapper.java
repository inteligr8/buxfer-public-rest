package com.inteligr8.buxfer;

import com.inteligr8.buxfer.model.Transaction;

public class TransactionWrapper {

	private final ParsedTransaction ptx;
	private final PartiallyReconciledTransaction prtx;
	private double outstandingSecurities;
	private double outstandingValue = 0.0;
	private float splitRatio = 1f;
	
	public TransactionWrapper(ParsedTransaction ptx) {
		this.ptx = ptx;
		
		if (ptx instanceof PartiallyReconciledTransaction) {
			this.prtx = (PartiallyReconciledTransaction)ptx;
			if (this.prtx.getUnsoldSecurities() != null)
				this.outstandingSecurities = this.prtx.getUnsoldSecurities().doubleValue();
		} else {
			this.prtx = null;
		}
		
		this.outstandingValue = ptx.getTransaction().getAmount();
		if (this.outstandingSecurities == 0.0) {
			this.outstandingSecurities = ptx.getSecurities();
		} else {
			double frac = this.outstandingSecurities / ptx.getSecurities();
			this.outstandingValue = Math.round(this.outstandingValue * frac * 10000L) / 10000.0;
		}
	}
	
	public ParsedTransaction getParsedTransaction() {
		return this.ptx;
	}
	
	public Transaction getTransaction() {
		return this.ptx.getTransaction();
	}
	
	public String getDescription() {
		if (this.outstandingSecurities == this.getParsedTransaction().getSecurities() || this.outstandingSecurities == 0.0) {
			return this.ptx.getNormalizedDescription();
		} else {
			double outstandingSecurities = this.outstandingSecurities / this.splitRatio;
			if (outstandingSecurities == this.getParsedTransaction().getSecurities()) {
				return this.ptx.getNormalizedDescription();
			} else {
				return this.ptx.getNormalizedDescription() + "; " + NumberFormatFactory.getSecuritiesFormatter().format(outstandingSecurities) + " unsold";
			}
		}
	}
	
	public double getSplitAdjustedSecurities() {
		if (this.splitRatio == 1f) {
			return this.ptx.getSecurities();
		} else {
			return Math.round(this.ptx.getSecurities() * this.splitRatio);
		}
	}
	
	public double getOutstandingSecurities() {
		return this.outstandingSecurities;
	}
	
	public double getOutstandingValue() {
		return this.outstandingValue;
	}
	
	public double decrementOutstandingSecurities(double securities) {
		if (this.outstandingSecurities < securities)
			throw new IllegalStateException();

		this.outstandingSecurities -= securities;
		if (this.outstandingSecurities == 0) {
			double value = this.outstandingValue;
			this.outstandingValue = 0.0;
			return value;
		}
		
		double frac = 1.0;
		double value = this.ptx.getTransaction().getAmount();
		if (securities < this.getSplitAdjustedSecurities()) {
			frac = 1.0 * securities / this.getSplitAdjustedSecurities();
			value *= frac;
		}
		
		this.outstandingValue -= value;
		return value;
	}
	
	public void recordSplit(float ratio) {
		this.splitRatio *= ratio;
		this.outstandingSecurities *= ratio;
	}

}
