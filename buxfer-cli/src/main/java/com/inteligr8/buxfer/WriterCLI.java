package com.inteligr8.buxfer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.buxfer.api.CommandApi;
import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.TransactionsResponse;

public class WriterCLI {
	
	private static final Logger logger = LoggerFactory.getLogger(WriterCLI.class);
	
	private enum Format {
		
		Json,
		Csv,
		MyStockPortfolioCsv("my-stock-portfolio-csv");
		
		private final String key;
		private Format() {
			this.key = this.toString().toLowerCase();
		}

		private Format(String key) {
			this.key = key;
		}
		
		public static Format valueByKey(String key) {
			for (Format format : Format.values())
				if (format.key.equals(key))
					return format;
			return Format.valueOf(key);
		}
	}
	
	public static void main(String[] args) throws ParseException, IOException, InterruptedException {
		Options options = buildOptions();
		String extraCommand = "[options] \"accountName\"";
		List<String> extraLines = Arrays.asList("accountName: A Buxfer account name");
		
		CommandLine cli = new DefaultParser().parse(options, args);
		if (cli.getArgList().isEmpty()) {
			CLI.help(options, extraCommand, extraLines, "An account name is required");
			return;
		} else if (cli.hasOption("help")) {
			CLI.help(options, extraCommand, extraLines);
			return;
		}

		String buxferAccountName = determineBuxferAccountName(cli);
		CommandApi api = findBuxferApi(cli);

		File file = determineFile(cli);
		FileOutputStream fostream = new FileOutputStream(file);
		BufferedOutputStream bostream = new BufferedOutputStream(fostream);
		try {
			Observer<Transaction> observer = determineTransactionWriter(cli, bostream);
		
			searchTransactions(api, buxferAccountName, observer);
		} finally {
			bostream.close();
		}
	}
	
	private static void searchTransactions(CommandApi api, String buxferAccountName, Observer<Transaction> observer) throws IOException, InterruptedException {
		long total = -1L;
		int pages = 1;
		for (int p = 1; p <= pages; p++) {
			TransactionsResponse response = api.getTransactionsInAccount(buxferAccountName, null, null, null, 1).getResponse();
			if (total < 0L) {
				total = response.getTotalItems();
				pages = (int)(response.getTotalItems() / 100L);
			} else if (total != response.getTotalItems()) {
				logger.error("Transaction count changed while processing");
				return;
			}
			
			for (Transaction tx : response.getItems())
				observer.observed(tx);
		}
	}
	
	private static Options buildOptions() {
		return new Options()
				.addOption(new Option("u", "email", true, "A Buxfer email for authentication"))
				.addOption(new Option("p", "password", true, "A Buxfer password for authentication"))
				.addOption(new Option("t", "format", true, "The output format [json | csv | my-stock-portfolio-csv]"))
				.addOption(new Option("f", "file", true, "The output filename"))
				.addOption(new Option(null, "portfolio-name", true, "The portfolio name where applicable"));
	}
	
	private static String determineBuxferAccountName(CommandLine cli) {
		return cli.getArgList().iterator().next();
	}
	
	private static File determineFile(CommandLine cli) {
		Format format = Format.valueByKey(cli.getOptionValue("format", "json"));
		
		String filename = cli.hasOption("file") ? cli.getOptionValue("file") : "portfolio";
		if (filename.lastIndexOf('.') < 0) {
			switch (format) {
				case Json:
					filename += ".json";
					break;
				default:
					filename += ".csv";
			}
		}
		return new File(filename);
	}
	
	private static Observer<Transaction> determineTransactionWriter(CommandLine cli, OutputStream ostream) {
		Format format = Format.valueByKey(cli.getOptionValue("format", "json"));
		
		switch (format) {
			case Json:
				return new JsonTransactionWriter(ostream);
			case MyStockPortfolioCsv:
				return new MyStockPortfolioCsvTransactionWriter(ostream);
			default:
				return new CsvTransactionWriter(ostream);
		}
	}
	
	private static CommandApi findBuxferApi(CommandLine cli) {
		BuxferClientConfiguration config = new BuxferClientConfiguration();
		config.setBaseUrl("https://www.buxfer.com");

		if (cli.hasOption("email"))
			config.setAuthEmail(cli.getOptionValue("email"));
		if (cli.hasOption("password"))
			config.setAuthPassword(cli.getOptionValue("password"));

		BuxferClientJerseyImpl client = new BuxferClientJerseyImpl(config);
		return client.getApi().getCommandApi();
	}

}
