package com.inteligr8.buxfer;

import com.inteligr8.buxfer.api.CommandApi;
import com.inteligr8.buxfer.api.SecurityApi;

/**
 * This interface consolidates the JAX-RS APIs available in the Buxfer Public
 * ReST API.
 * 
 * @author	 brian@inteligr8.com
 */
public interface BuxferPublicRestApi {
	
	SecurityApi getSecurityApi();
	
	CommandApi getCommandApi();

}
