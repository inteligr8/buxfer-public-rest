package com.inteligr8.buxfer.api;

import java.time.LocalDate;
import java.time.YearMonth;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.inteligr8.buxfer.model.AccountsResponse;
import com.inteligr8.buxfer.model.BudgetsResponse;
import com.inteligr8.buxfer.model.ContactsResponse;
import com.inteligr8.buxfer.model.GroupsResponse;
import com.inteligr8.buxfer.model.LoansResponse;
import com.inteligr8.buxfer.model.RemindersResponse;
import com.inteligr8.buxfer.model.Response;
import com.inteligr8.buxfer.model.TagsResponse;
import com.inteligr8.buxfer.model.Transaction.Status;
import com.inteligr8.buxfer.model.TransactionResponse;
import com.inteligr8.buxfer.model.TransactionsResponse;

@Path("/api")
public interface CommandApi {

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactions(
    		@QueryParam("startDate") LocalDate startDate,
    		@QueryParam("endDate") LocalDate endDate,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactionsInTag(
    		@QueryParam("tagName") String tagName,
    		@QueryParam("startDate") LocalDate startDate,
    		@QueryParam("endDate") LocalDate endDate,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactionsInAccount(
    		@QueryParam("accountName") String accountName,
    		@QueryParam("startDate") LocalDate startDate,
    		@QueryParam("endDate") LocalDate endDate,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactions(
    		@QueryParam("accountName") String accountName,
    		@QueryParam("tagName") String tagName,
    		@QueryParam("startDate") LocalDate startDate,
    		@QueryParam("endDate") LocalDate endDate,
    		@QueryParam("budgetName") String budgetName,
    		@QueryParam("contactName") String contactName,
    		@QueryParam("groupName") String groupName,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactions(
    		@QueryParam("accountName") String accountName,
    		@QueryParam("tagName") String tagName,
    		@QueryParam("startDate") YearMonth month,
    		@QueryParam("budgetName") String budgetName,
    		@QueryParam("contactName") String contactName,
    		@QueryParam("groupName") String groupName,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactionsByIds(
    		@QueryParam("accountId") Long accountId,
    		@QueryParam("tagId") Long tagId,
    		@QueryParam("startDate") LocalDate startDate,
    		@QueryParam("endDate") LocalDate endDate,
    		@QueryParam("budgetId") Long budgetId,
    		@QueryParam("contactId") Long contactId,
    		@QueryParam("groupId") Long groupId,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactionsByIds(
    		@QueryParam("accountId") Long accountId,
    		@QueryParam("tagId") Long tagId,
    		@QueryParam("startDate") YearMonth month,
    		@QueryParam("budgetId") Long budgetId,
    		@QueryParam("contactId") Long contactId,
    		@QueryParam("groupId") Long groupId,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactions(
    		@QueryParam("accountId") Long accountId,
    		@QueryParam("accountName") String accountName,
    		@QueryParam("tagId") Long tagId,
    		@QueryParam("tagName") String tagName,
    		@QueryParam("startDate") LocalDate startDate,
    		@QueryParam("endDate") LocalDate endDate,
    		@QueryParam("budgetId") Long budgetId,
    		@QueryParam("budgetName") String budgetName,
    		@QueryParam("contactId") Long contactId,
    		@QueryParam("contactName") String contactName,
    		@QueryParam("groupId") Long groupId,
    		@QueryParam("groupName") String groupName,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @GET
    @Path("/transactions")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionsResponse> getTransactions(
    		@QueryParam("accountId") Long accountId,
    		@QueryParam("accountName") String accountName,
    		@QueryParam("tagId") Long tagId,
    		@QueryParam("tagName") String tagName,
    		@QueryParam("month") YearMonth month,
    		@QueryParam("budgetId") Long budgetId,
    		@QueryParam("budgetName") String budgetName,
    		@QueryParam("contactId") Long contactId,
    		@QueryParam("contactName") String contactName,
    		@QueryParam("groupId") Long groupId,
    		@QueryParam("groupName") String groupName,
    		@QueryParam("status") Status status,
    		@QueryParam("page") int page);

    @POST
    @Path("/transaction_add")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionResponse> addTransaction(
    		@FormParam("type") String type,
    		@FormParam("accountId") long accountId,
    		@FormParam("date") LocalDate date,
    		@FormParam("description") String description,
    		@FormParam("amount") double amount,
    		@FormParam("tags") String tags,
    		@FormParam("status") Status status);

    @POST
    @Path("/transaction_add")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionResponse> addTransaction(
    		@FormParam("type") String type,
    		@FormParam("fromAccountId") Long fromAccountId,
    		@FormParam("toAccountId") Long toAccountId,
    		@FormParam("date") LocalDate date,
    		@FormParam("description") String description,
    		@FormParam("amount") double amount,
    		@FormParam("tags") String tags,
    		@FormParam("status") Status status);

    @POST
    @Path("/transaction_edit")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionResponse> editTransaction(
    		@FormParam("id") long id,
    		@FormParam("description") String description,
    		@FormParam("tags") String tags,
    		@FormParam("status") Status status);

    @POST
    @Path("/transaction_edit")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionResponse> editTransaction(
    		@FormParam("id") long id,
    		@FormParam("type") String type,
    		@FormParam("description") String description,
    		@FormParam("tags") String tags,
    		@FormParam("status") Status status);

    @POST
    @Path("/transaction_edit")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionResponse> editTransaction(
    		@FormParam("id") long id,
    		@FormParam("type") String type,
    		@FormParam("accountId") Long accountId,
    		@FormParam("date") LocalDate date,
    		@FormParam("description") String description,
    		@FormParam("amount") Double amount,
    		@FormParam("tags") String tags,
    		@FormParam("status") Status status);

    @POST
    @Path("/transaction_edit")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TransactionResponse> editTransaction(
    		@FormParam("id") long id,
    		@FormParam("type") String type,
    		@FormParam("fromAccountId") Long fromAccountId,
    		@FormParam("toAccountId") Long toAccountId,
    		@FormParam("date") LocalDate date,
    		@FormParam("description") String description,
    		@FormParam("amount") Double amount,
    		@FormParam("tags") String tags,
    		@FormParam("status") Status status);

    @POST
    @Path("/transaction_delete")
    @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
    public void deleteTransaction(
    		@FormParam("id") long id);

    @GET
    @Path("/accounts")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<AccountsResponse> getAccounts();

    @GET
    @Path("/loans")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<LoansResponse> getLoans();

    @GET
    @Path("/tags")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<TagsResponse> getTags();

    @GET
    @Path("/budgets")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<BudgetsResponse> getBudgets();

    @GET
    @Path("/reminders")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<RemindersResponse> getReminders();

    @GET
    @Path("/groups")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<GroupsResponse> getGroups();

    @GET
    @Path("/contacts")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response<ContactsResponse> getContacts();

}
