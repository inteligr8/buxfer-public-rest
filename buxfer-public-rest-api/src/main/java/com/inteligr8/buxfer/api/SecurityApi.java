package com.inteligr8.buxfer.api;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.inteligr8.buxfer.model.Response;
import com.inteligr8.buxfer.model.TokenResponse;

@Path("/api")
public interface SecurityApi {

    @POST
    @Path("/login")
    @Produces({ "application/json" })
    public Response<TokenResponse> login(
    		@FormParam("email") String email,
    		@FormParam("password") String password);

}
