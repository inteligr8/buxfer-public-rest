package com.inteligr8.buxfer.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {
 * 		"id": long,
 * 		"name": string,
 * 		"bank": string,
 * 		"balance": double,
 * 		"currency": string,       // e.g. USD
 * 		"lastSynced": date/time,  // e.g. 2021-12-20 23:59:04
 * 		"availableCredit": double,
 * 		"apr": double,            // e.g. 12.99
 * 		"totalCreditLine": double
 * }
 * 
 * @author brian@inteligr8.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account extends NamedItem {
	
	@JsonProperty
	private String bank;
	
	@JsonProperty
	private Double balance;

	@JsonProperty
	private String currency;
	
	@JsonProperty("lastSynced")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime lastSyncd;
	
	@JsonProperty
	private Double apr;

	@JsonProperty
	private Double availableCredit;

	@JsonProperty("totalCreditLine")
	private Double totalCredit;
	
	

	public String getBank() {
		return this.bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public Double getBalance() {
		return this.balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}
	
	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public LocalDateTime getLastSyncd() {
		return this.lastSyncd;
	}

	public void setLastSyncd(LocalDateTime lastSyncd) {
		this.lastSyncd = lastSyncd;
	}

	public Double getApr() {
		return this.apr;
	}

	public void setApr(Double apr) {
		this.apr = apr;
	}

	public Double getAvailableCredit() {
		return this.availableCredit;
	}

	public void setAvailableCredit(Double availableCredit) {
		this.availableCredit = availableCredit;
	}

	public Double getTotalCredit() {
		return this.totalCredit;
	}

	public void setTotalCredit(Double totalCredit) {
		this.totalCredit = totalCredit;
	}

}
