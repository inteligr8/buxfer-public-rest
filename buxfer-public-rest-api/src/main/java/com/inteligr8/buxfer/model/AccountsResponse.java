package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountsResponse extends ArrayResponse<Account> {
	
	@JsonProperty("accounts")
	private List<Account> items;
	
	
	
	public List<Account> getItems() {
		return this.items;
	}
	
	public void setItems(List<Account> items) {
		this.items = items;
	}

}
