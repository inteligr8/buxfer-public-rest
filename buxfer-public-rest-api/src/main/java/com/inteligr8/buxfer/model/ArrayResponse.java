package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class ArrayResponse<T> extends BaseResponse {
	
	public abstract List<T> getItems();
	
	public abstract void setItems(List<T> items);

}
