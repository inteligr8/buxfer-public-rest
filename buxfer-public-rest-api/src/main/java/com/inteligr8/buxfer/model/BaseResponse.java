package com.inteligr8.buxfer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("response")
public class BaseResponse {
	
	public enum Status {
		OK,
		ERROR
	}
	
	@JsonProperty
	private Status status;
	
	@JsonProperty(value = "error_description", required = false)
	private String errorDesc;
	
	@JsonProperty(required = false)
	private Error error;
	
	
	
	public Status getStatus() {
		return this.status;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	public String getErrorDescription() {
		return this.errorDesc;
	}
	
	public void setErrorDescription(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	
	public Error getError() {
		return this.error;
	}
	
	public void setError(Error error) {
		this.error = error;
	}
	
	@JsonIgnore
	public String getErrorDescriptionOrMessage() {
		if (this.error != null && this.error.getMessage() != null) {
			return this.error.getMessage();
		} else {
			return this.errorDesc;
		}
	}

}
