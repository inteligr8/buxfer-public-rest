package com.inteligr8.buxfer.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {
 * 		"id": long
 * 		"name": string,
 * 		"editMode": string,
 * 		"periodSize": int,
 * 		"periodUnit": string,  // e.g. week
 * 		"startDate": date,     // e.g. 2020-08-23
 * 		"stopDate": date,
 * 		"budgetId": long,
 * 		"type": int,
 * 		"tagId": long,
 * 		"tag": { see Tag doc },
 * 		"limit": double,
 * 		"spent": double,
 * 		"balance": double,
 * 		"period": string,     // e.g. "19 - 25 Dec",
 * 		"isRolledOver": int,
 * 		"eventId": long,
 * }
 * 
 * @author brian@inteligr8.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Budget extends NamedItem {
	
	@JsonProperty
	private String editMode;
	
	@JsonProperty
	private Double limit;
	
	@JsonProperty
	private Double spent;
	
	@JsonProperty
	private Double balance;
	
	@JsonProperty
	private Double remaining;
	
	@JsonProperty
	private String period;
	
	@JsonProperty
	private int periodSize;
	
	@JsonProperty
	private String periodUnit;

	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate startDate;

	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate endDate;
	
	@JsonProperty
	private Long budgetId;
	
	@JsonProperty
	private Long tagId;
	
	@JsonProperty
	private Tag tag;
	
	@JsonProperty
	private Long eventId;
	
	@JsonProperty("isRolledOver")
	private Integer rolledOver;
	
	

	public String getEditMode() {
		return this.editMode;
	}

	public void setEditMode(String editMode) {
		this.editMode = editMode;
	}

	public Double getLimit() {
		return this.limit;
	}

	public void setLimit(Double limit) {
		this.limit = limit;
	}

	public Double getSpent() {
		return this.spent;
	}

	public void setSpent(Double spent) {
		this.spent = spent;
	}

	public Double getBalance() {
		return this.balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getRemaining() {
		return this.remaining;
	}

	public void setRemaining(Double remaining) {
		this.remaining = remaining;
	}

	public String getPeriod() {
		return this.period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public int getPeriodSize() {
		return this.periodSize;
	}

	public void setPeriodSize(int periodSize) {
		this.periodSize = periodSize;
	}

	public String getPeriodUnit() {
		return this.periodUnit;
	}

	public void setPeriodUnit(String periodUnit) {
		this.periodUnit = periodUnit;
	}

	public LocalDate getStartDate() {
		return this.startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return this.endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Long getBudgetId() {
		return this.budgetId;
	}

	public void setBudgetId(Long budgetId) {
		this.budgetId = budgetId;
	}

	public Long getTagId() {
		return this.tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public Tag getTag() {
		return this.tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public Long getEventId() {
		return this.eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public boolean isRolledOver() {
		return this.rolledOver != null && this.rolledOver != 0;
	}

	public void setRolledOver(boolean rolledOver) {
		this.rolledOver = rolledOver ? 1 : 0;
	}

}
