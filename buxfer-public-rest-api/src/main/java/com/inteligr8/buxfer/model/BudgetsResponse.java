package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BudgetsResponse extends ArrayResponse<Budget> {
	
	@JsonProperty("budgets")
	private List<Budget> items;
	
	
	
	public List<Budget> getItems() {
		return this.items;
	}
	
	public void setItems(List<Budget> items) {
		this.items = items;
	}

}
