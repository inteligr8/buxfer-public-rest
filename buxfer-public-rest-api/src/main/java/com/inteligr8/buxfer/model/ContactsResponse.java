package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactsResponse extends ArrayResponse<Person> {
	
	@JsonProperty("contacts")
	private List<Person> items;
	
	
	
	public List<Person> getItems() {
		return this.items;
	}
	
	public void setItems(List<Person> items) {
		this.items = items;
	}

}
