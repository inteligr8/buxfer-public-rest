package com.inteligr8.buxfer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {
	
	@JsonProperty
	private String type;
	
	@JsonProperty
	private String message;
	
	
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

}
