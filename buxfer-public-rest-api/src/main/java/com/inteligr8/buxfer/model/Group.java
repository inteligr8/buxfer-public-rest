package com.inteligr8.buxfer.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Group extends NamedItem {
	
	@JsonProperty
	private boolean consolidated;
	
	@JsonProperty
	private Map<String, Person> members;
	
	

	public boolean isConsolidated() {
		return this.consolidated;
	}

	public void setConsolidated(boolean consolidated) {
		this.consolidated = consolidated;
	}

	public Map<String, Person> getMembers() {
		return this.members;
	}

	public void setMembers(Map<String, Person> members) {
		this.members = members;
	}

}
