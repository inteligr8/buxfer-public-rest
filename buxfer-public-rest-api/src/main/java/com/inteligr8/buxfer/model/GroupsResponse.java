package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupsResponse extends ArrayResponse<Group> {
	
	@JsonProperty("reminders")
	private List<Group> items;
	
	
	
	public List<Group> getItems() {
		return this.items;
	}
	
	public void setItems(List<Group> items) {
		this.items = items;
	}

}
