package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoansResponse extends ArrayResponse<Loan> {
	
	@JsonProperty("loans")
	private List<Loan> items;
	
	
	
	public List<Loan> getItems() {
		return this.items;
	}
	
	public void setItems(List<Loan> items) {
		this.items = items;
	}

}
