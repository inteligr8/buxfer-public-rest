package com.inteligr8.buxfer.model;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {
 * 		"id": long,
 * 		"name": string,
 * 		"nextExecution": date,
 * 		"dueDateDescription": string,
 * 		"numDaysForDueDate": int,
 * 		"tags": [ see Tag doc ]
 * 		"editMode": string,       // e.g. schedule_all
 * 		"periodSize": int,
 * 		"periodUnit": string,     // e.g. month
 * 		"startDate": date,
 * 		"stopDate": date,
 * 		"description": string,
 * 		"type": string,           // e.g. transfer
 * 		"transactionType": int,
 * 		"amount": double,
 * 		"fromAccountId": long,
 * 		"toAccountId": long
 * }
 * 
 * @author brian@inteligr8.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reminder extends NamedItem {
	
	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate nextExecution;
	
	@JsonProperty
	private String dueDateDescription;
	
	@JsonProperty
	private Integer numDaysForDueDate;
	
	@JsonProperty
	private List<Tag> tags;
	
	@JsonProperty
	private String editMode;
	
	@JsonProperty
	private Integer periodSize;
	
	@JsonProperty
	private String periodUnit;
	
	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate startDate;
	
	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate endDate;
	
	@JsonProperty
	private String description;
	
	@JsonProperty
	private String type;
	
	@JsonProperty
	private Integer transactionType;
	
	@JsonProperty
	private Double amount;
	
	@JsonProperty
	private Long accountId;
	
	@JsonProperty
	private Long fromAccountId;
	
	@JsonProperty
	private Long toAccountId;
	
	

	public LocalDate getNextExecution() {
		return this.nextExecution;
	}

	public void setNextExecution(LocalDate nextExecution) {
		this.nextExecution = nextExecution;
	}

	public String getDueDateDescription() {
		return this.dueDateDescription;
	}

	public void setDueDateDescription(String dueDateDescription) {
		this.dueDateDescription = dueDateDescription;
	}

	public Integer getNumDaysForDueDate() {
		return this.numDaysForDueDate;
	}

	public void setNumDaysForDueDate(Integer numDaysForDueDate) {
		this.numDaysForDueDate = numDaysForDueDate;
	}

	public List<Tag> getTags() {
		return this.tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public String getEditMode() {
		return this.editMode;
	}

	public void setEditMode(String editMode) {
		this.editMode = editMode;
	}

	public Integer getPeriodSize() {
		return this.periodSize;
	}

	public void setPeriodSize(Integer periodSize) {
		this.periodSize = periodSize;
	}

	public String getPeriodUnit() {
		return this.periodUnit;
	}

	public void setPeriodUnit(String periodUnit) {
		this.periodUnit = periodUnit;
	}

	public LocalDate getStartDate() {
		return this.startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return this.endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTransactionType() {
		return this.transactionType;
	}

	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getFromAccountId() {
		return this.fromAccountId;
	}

	public void setFromAccountId(Long fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public Long getToAccountId() {
		return this.toAccountId;
	}

	public void setToAccountId(Long toAccountId) {
		this.toAccountId = toAccountId;
	}

}
