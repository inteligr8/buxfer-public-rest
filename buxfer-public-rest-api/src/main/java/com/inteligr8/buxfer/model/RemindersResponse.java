package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RemindersResponse extends ArrayResponse<Reminder> {
	
	@JsonProperty("reminders")
	private List<Reminder> items;
	
	
	
	public List<Reminder> getItems() {
		return this.items;
	}
	
	public void setItems(List<Reminder> items) {
		this.items = items;
	}

}
