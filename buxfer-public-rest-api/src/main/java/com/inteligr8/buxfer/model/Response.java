package com.inteligr8.buxfer.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response<T> {
	
	@JsonProperty
	private T response;
	
	
	
	public T getResponse() {
		return this.response;
	}
	
	public void setResponse(T response) {
		this.response = response;
	}

}
