package com.inteligr8.buxfer.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {
 * 		"id": long,
 * 		"name": string,          // full tag path; "\/" delimited
 * 		"relativeName": string,  // leave in tag path
 * 		"parentId": long
 * }
 * 
 * @author brian@inteligr8.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tag extends NamedItem {

	@JsonProperty
	private String relativeName;
	
	@JsonProperty
	private Long parentId;
	
	
	
	public String getPath() {
		return this.getName().replace(" \\/ ", " / ");
	}
	
	public String getRelativeName() {
		return this.relativeName;
	}

	public void setRelativeName(String relativeName) {
		this.relativeName = relativeName;
	}

	public Long getParentId() {
		return this.parentId;
	}
	
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

}
