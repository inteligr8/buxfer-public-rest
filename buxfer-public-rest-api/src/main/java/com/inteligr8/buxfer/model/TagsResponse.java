package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TagsResponse extends ArrayResponse<Tag> {
	
	@JsonProperty("tags")
	private List<Tag> items;
	
	
	
	public List<Tag> getItems() {
		return this.items;
	}
	
	public void setItems(List<Tag> items) {
		this.items = items;
	}

}
