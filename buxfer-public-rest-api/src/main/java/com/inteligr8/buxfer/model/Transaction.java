package com.inteligr8.buxfer.model;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * {
 * 		"id": long,
 * 		"description": string,
 * 		"date": date,
 * 		"type": string,            // e.g. expense (depreciated?)
 * 		"transactionType": string  // e.g. expense, investment purchase
 * 		"amount": double,
 * 		"expenseAmount": double,   // signed (negative for income/refund)
 * 		"accountId": long,
 * 		"accountName": string,
 * 		"fromAccouint": {
 * 			"id": long,
 * 			"name": string
 * 		},
 * 		"toAccouint": {
 * 			"id": long,
 * 			"name": string
 * 		},
 * 		"tags": string,            // e.g. Tag1, Tag2 (depreciated?)
 * 		"tagNames": [ string, ... ],
 * 		"status": string           // e.g. cleared
 * 		"isFutureDated": boolean,
 * 		"isPending": boolean
 * }
 * 
 * @author brian@inteligr8.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction extends Item {
	
	public enum Status {
		@JsonProperty("pending")
		Pending,
		@JsonProperty("reconciled")
		Reconciled,
		@JsonProperty("cleared")
		Cleared
	}
	
	private static final Map<String, Type> incomingTypeMap = new HashMap<>(20);
	
	@JsonDeserialize(using = Type.Deserializer.class)
	@JsonSerialize(using = Type.Serializer.class)
	public enum Type {
		Income("income"),
		Expense("expense"),
		Refund("refund"),
		Transfer("transfer"),
		Buy("investment purchase", "investment_buy"),
		Sell("investment sale", "investment_sell"),
		Dividend("dividend", "investment_dividend"),
		CapitalGain("capital gain", "capital_gain"),
		SharedBill("sharedBill"),
		PaidForFriend("paidForFriend"),
		Loan("loan"),
		Settlement("settlement");
		
		private final String outgoingValue;
		
		private Type(String value) {
			this(value.toLowerCase(), value);
		}
		
		private Type(String incomingValue, String outgoingValue) {
			this.outgoingValue = outgoingValue;
			incomingTypeMap.put(incomingValue, this);
		}
		
		@JsonValue
		public String getOutgoingValue() {
			return this.outgoingValue;
		}
		
		@JsonCreator
		public static Type fromIncomingValue(String value) {
			return incomingTypeMap.get(value);
		}
		
		// FIXME the serializer is not getting called when expected; but deserializer is getting called and works
		public static class Serializer extends JsonSerializer<Type> {
			public Serializer() {}
			@Override
			public void serialize(Type value, JsonGenerator gen, SerializerProvider provider) throws IOException {
				gen.writeString(value.outgoingValue);
			}
		}
		
		public static class Deserializer extends JsonDeserializer<Type> {
			public Deserializer() {}
			@Override
			public Type deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				String value = p.getText();
				return Type.fromIncomingValue(value);
			}
		}
		
	}
	
	public enum IncomingType {
		@JsonProperty("income")
		Income,
		@JsonProperty("expense")
		Expense,
		@JsonProperty("refund")
		Refund,
		@JsonProperty("transfer")
		Transfer,
		@JsonProperty("investment purchase")
		Buy,
		@JsonProperty("investment sale")
		Sell,
		@JsonProperty("dividend")
		Dividend,
		@JsonProperty("capital gain")
		CapitalGain,
		@JsonProperty("sharedbill")
		SharedBill,
		@JsonProperty("paidforfriend")
		PaidForFriend,
		@JsonProperty("loan")
		Loan,
		@JsonProperty("settlement")
		Settlement;
		
		public OutgoingType toOutgoingType() {
			return OutgoingType.valueOf(this.toString());
		}
	}
	
	public enum OutgoingType {
		@JsonProperty("income")
		Income,
		@JsonProperty("expense")
		Expense,
		@JsonProperty("refund")
		Refund,
		@JsonProperty("transfer")
		Transfer,
		@JsonProperty("investment_buy")
		Buy,
		@JsonProperty("investment_sell")
		Sell,
		@JsonProperty("investment_dividend")
		Dividend,
		@JsonProperty("capital_gain")
		CapitalGain,
		@JsonProperty("sharedBill")
		SharedBill,
		@JsonProperty("paidForFriend")
		PaidForFriend,
		@JsonProperty("loan")
		Loan,
		@JsonProperty("settlement")
		Settlement;
		
		public IncomingType toIncomingType() {
			return IncomingType.valueOf(this.toString());
		}
	}
	
	@JsonProperty
	private String description;
	
	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate date;
	
	@JsonProperty
	private Type transactionType;
	
	@JsonProperty
	private Double amount;
	
	@JsonProperty
	private Double expenseAmount;
	
	@JsonProperty
	private Long accountId;
	
	@JsonProperty
	private String accountName;
	
	@JsonProperty
	private NamedItem fromAccount;
	
	@JsonProperty
	private NamedItem toAccount;
	
	@JsonProperty
	private List<String> tagNames;
	
	@JsonProperty
	private Status status;
	
	@JsonProperty
	private Boolean isFutureDated;
	
	@JsonProperty
	private Boolean isPending;
	
	

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDate() {
		return this.date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Type getType() {
		return this.transactionType;
	}

	public void setType(Type type) {
		this.transactionType = type;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getExpenseAmount() {
		return this.expenseAmount;
	}

	public void setExpenseAmount(Double expenseAmount) {
		this.expenseAmount = expenseAmount;
	}

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return this.accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public NamedItem getFromAccount() {
		return this.fromAccount;
	}

	public void setFromAccount(NamedItem fromAccount) {
		this.fromAccount = fromAccount;
	}

	public NamedItem getToAccount() {
		return this.toAccount;
	}

	public void setToAccount(NamedItem toAccount) {
		this.toAccount = toAccount;
	}

	public List<String> getTagNames() {
		return this.tagNames;
	}

	public void setTagNames(List<String> tagNames) {
		this.tagNames = tagNames;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Boolean getIsFutureDated() {
		return this.isFutureDated;
	}

	public void setIsFutureDated(Boolean isFutureDated) {
		this.isFutureDated = isFutureDated;
	}

	public Boolean getIsPending() {
		return this.isPending;
	}

	public void setIsPending(Boolean isPending) {
		this.isPending = isPending;
	}

}
