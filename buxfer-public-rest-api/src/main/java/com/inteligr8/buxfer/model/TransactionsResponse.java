package com.inteligr8.buxfer.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionsResponse extends ArrayResponse<Transaction> {
	
	@JsonProperty("transactions")
	private List<Transaction> items;
	
	@JsonProperty("numTransactions")
	private long totalItems;
	
	
	
	public long getTotalItems() {
		return this.totalItems;
	}
	
	public void setTotalItems(long totalItems) {
		this.totalItems = totalItems;
	}
	
	public List<Transaction> getItems() {
		return this.items;
	}
	
	public void setItems(List<Transaction> items) {
		this.items = items;
	}

}
