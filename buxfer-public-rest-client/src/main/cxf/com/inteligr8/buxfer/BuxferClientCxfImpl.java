package com.inteligr8.buxfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.inteligr8.rs.ClientCxfConfiguration;
import com.inteligr8.rs.ClientCxfImpl;

/**
 * This class provides a POJO &amp; Spring-based implementation of the Apache
 * CXF client.  You can use it outside of the Spring context, but you will need
 * the spring-context and spring-beans libraries in your non-Spring
 * application.
 * 
 * @author brian@inteligr8.com
 */
@Component("buxfer.client")
@Lazy
public class BuxferClientCxfImpl extends ClientCxfImpl {
	
	@Autowired
	private BuxferClientConfiguration config;
	
	/**
	 * This constructor is for Spring use.
	 */
	protected BuxferClientCxfImpl() {
	}
	
	/**
	 * This constructor is for POJO use.
	 * @param config
	 */
	public BuxferClientCxfImpl(BuxferClientConfiguration config) {
		this.config = config;
	}
	
	@Override
	protected ClientCxfConfiguration getConfig() {
		return this.config;
	}

}
