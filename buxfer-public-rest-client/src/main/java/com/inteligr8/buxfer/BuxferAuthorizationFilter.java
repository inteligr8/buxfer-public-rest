package com.inteligr8.buxfer;

import java.io.IOException;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.buxfer.model.Response;
import com.inteligr8.buxfer.model.TokenResponse;
import com.inteligr8.rs.AuthorizationFilter;

public class BuxferAuthorizationFilter implements AuthorizationFilter {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String email;
	private String password;
	private String token;
	
	public BuxferAuthorizationFilter(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		if (requestContext.getUri().getPath().endsWith("/login"))
			return;
		
		if (this.token == null)
			this.requestToken(requestContext);
		
		requestContext.setUri(
			UriBuilder.fromUri(requestContext.getUri())
				.queryParam("token", this.token)
				.build());
	}
	
	protected void requestToken(ClientRequestContext requestContext) {
		UriBuilder loginUri = UriBuilder.fromUri(requestContext.getUri())
				.replacePath("/api/login")
				.replaceQuery(null);
		
		Form form = new Form();
		form.param("email", this.email);
		form.param("password", this.password);
		Entity<Form> formEntity = Entity.form(form);
		
		GenericType<Response<TokenResponse>> responseType = new GenericType<Response<TokenResponse>>() {};
		
		try {
			TokenResponse response = requestContext.getClient()
				.target(loginUri)
					.request()
						.accept(MediaType.APPLICATION_JSON)
						.post(formEntity, responseType).getResponse();
			
			if (!com.inteligr8.buxfer.model.BaseResponse.Status.OK.equals(response.getStatus()))
				throw new NotAuthorizedException(response.getErrorDescriptionOrMessage(), response);
			this.token = response.getToken();
			this.logger.debug("received access token: {} = > {}", this.email, this.token);
		} catch (NotAuthorizedException nae) {
			throw nae;
		} catch (WebApplicationException wae) {
			throw new NotAuthorizedException("Indirect due to non-authorization failure: [" + wae.getResponse().getStatus() + "]", wae);
		}
	}

}
