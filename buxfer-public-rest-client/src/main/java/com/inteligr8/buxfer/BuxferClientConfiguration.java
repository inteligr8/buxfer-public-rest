package com.inteligr8.buxfer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.inteligr8.rs.AuthorizationFilter;
import com.inteligr8.rs.ClientCxfConfiguration;
import com.inteligr8.rs.ClientJerseyConfiguration;

/**
 * This class provides a POJO &amp; Spring-based implementation of the
 * ClientConfiguration interface.  You can use it outside of the Spring
 * context, but you will need the spring-context and spring-beans libraries in
 * your non-Spring application.
 * 
 * @author brian@inteligr8.com
 */
@Configuration
@ComponentScan
public class BuxferClientConfiguration implements ClientCxfConfiguration, ClientJerseyConfiguration {

	@Value("${buxfer.service.baseUrl:https://www.buxfer.com}")
	private String baseUrl;

	@Value("${buxfer.service.security.auth.email}")
	private String authEmail;

	@Value("${buxfer.service.security.auth.password}")
	private String authPassword;
	
	@Value("${buxfer.service.cxf.defaultBusEnabled:true}")
	private boolean defaultBusEnabled;
	
	@Value("${buxfer.service.jersey.putBodyRequired:true}")
	private boolean putBodyRequired;

	public String getBaseUrl() {
		return this.baseUrl;
	}
	
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public String getAuthEmail() {
		return this.authEmail;
	}
	
	public void setAuthEmail(String authEmail) {
		this.authEmail = authEmail;
	}
	
	public String getAuthPassword() {
		return this.authPassword;
	}
	
	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}
	
	public boolean isUnwrapRootValueEnabled() {
		return true;
	}
	
	public boolean isDefaultBusEnabled() {
		return this.defaultBusEnabled;
	}

	public void setDefaultBusEnabled(boolean defaultBusEnabled) {
		this.defaultBusEnabled = defaultBusEnabled;
	}
	
	public boolean isPutBodyRequired() {
		return this.putBodyRequired;
	}

	public void setPutBodyRequired(boolean putBodyRequired) {
		this.putBodyRequired = putBodyRequired;
	}
	
	
	
	@Override
	public AuthorizationFilter createAuthorizationFilter() {
		return new BuxferAuthorizationFilter(this.getAuthEmail(), this.getAuthPassword());
	}

}
