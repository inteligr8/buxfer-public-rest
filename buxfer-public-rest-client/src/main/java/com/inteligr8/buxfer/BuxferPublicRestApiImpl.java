package com.inteligr8.buxfer;

import com.inteligr8.buxfer.api.CommandApi;
import com.inteligr8.buxfer.api.SecurityApi;
import com.inteligr8.rs.Client;

/**
 * This class serves as the entrypoint to the JAX-RS API for the Buxfer Public
 * ReST API.
 * 
 * @author brian@inteligr8.com
 */
public class BuxferPublicRestApiImpl implements BuxferPublicRestApi {
	
	private final Client client;
	
	public BuxferPublicRestApiImpl(Client client) {
		this.client = client;
	}
	
	protected final <T> T getApi(Class<T> apiClass) {
		return this.client.getApi(apiClass);
	}
	
	public SecurityApi getSecurityApi() {
		return this.client.getApi(SecurityApi.class);
	}
	
	public CommandApi getCommandApi() {
		return this.client.getApi(CommandApi.class);
	}

}
