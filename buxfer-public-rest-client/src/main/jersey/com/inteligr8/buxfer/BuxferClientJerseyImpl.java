package com.inteligr8.buxfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.inteligr8.rs.ClientJerseyConfiguration;
import com.inteligr8.rs.ClientJerseyImpl;

/**
 * This class provides a POJO &amp; Spring-based implementation of the Apache
 * CXF client.  You can use it outside of the Spring context, but you will need
 * the spring-context and spring-beans libraries in your non-Spring
 * application.
 * 
 * @author brian@inteligr8.com
 */
@Component("buxfer.client")
@Lazy
public class BuxferClientJerseyImpl extends ClientJerseyImpl {
	
	@Autowired
	private BuxferClientConfiguration config;
	
	private final BuxferPublicRestApi api;
	
	/**
	 * This constructor is for Spring use.
	 */
	protected BuxferClientJerseyImpl() {
		this.api = new BuxferPublicRestApiImpl(this);
	}
	
	/**
	 * This constructor is for POJO use.
	 * @param config
	 */
	public BuxferClientJerseyImpl(BuxferClientConfiguration config) {
		this.config = config;
		this.api = new BuxferPublicRestApiImpl(this);
	}
	
	@Override
	protected ClientJerseyConfiguration getConfig() {
		return this.config;
	}
	
	public BuxferPublicRestApi getApi() {
		return this.api;
	}

}
