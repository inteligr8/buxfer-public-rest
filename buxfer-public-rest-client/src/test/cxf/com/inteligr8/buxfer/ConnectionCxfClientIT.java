package com.inteligr8.buxfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.inteligr8.rs.ClientConfiguration;

@TestPropertySource(locations = {"/buxfer-personal.properties"})
@SpringJUnitConfig(classes = {BuxferClientConfiguration.class, BuxferClientCxfImpl.class})
public class ConnectionCxfClientIT extends ConnectionClientIT {
	
	@Autowired
	private BuxferClientCxfImpl client;
	
	@Override
	public BuxferPublicRestApi getClient() {
		return this.client.getApi(BuxferPublicRestApi.class);
	}
	
	@Override
	public ClientConfiguration getConfiguration() {
		return this.client.getConfig();
	}
	
}
