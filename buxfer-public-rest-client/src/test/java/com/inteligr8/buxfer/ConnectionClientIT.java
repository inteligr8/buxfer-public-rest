package com.inteligr8.buxfer;

import java.time.LocalDate;
import java.util.Collection;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIf;

import com.inteligr8.buxfer.api.CommandApi;
import com.inteligr8.buxfer.model.Account;
import com.inteligr8.buxfer.model.ArrayResponse;
import com.inteligr8.buxfer.model.BaseResponse;
import com.inteligr8.buxfer.model.BaseResponse.Status;
import com.inteligr8.buxfer.model.Budget;
import com.inteligr8.buxfer.model.Item;
import com.inteligr8.buxfer.model.NamedItem;
import com.inteligr8.buxfer.model.Reminder;
import com.inteligr8.buxfer.model.Tag;
import com.inteligr8.buxfer.model.Transaction;
import com.inteligr8.buxfer.model.TransactionsResponse;

public abstract class ConnectionClientIT extends ConditionalIT {
	
	public abstract BuxferPublicRestApi getClient();
	
	private CommandApi api;
	
	public boolean fullTest() {
		return false;
		//return this.hostExists();
	}
	
	@BeforeEach
	public void getApi() {
		this.api = this.getClient().getCommandApi();
	}
	
	@Test
	@EnabledIf("hostExists")
	public void testTransactions() {
		TransactionsResponse response = this.api.getTransactions(
				LocalDate.of(2021, 12, 1),
				LocalDate.of(2021, 12, 31),
				null,
				1
			).getResponse();
		this.assertArrayNotEmpty(response);
		Assertions.assertTrue(response.getTotalItems() > 0L);
		this.assertItems(response.getItems());
	}
	
	@Test
	@EnabledIf("hostExists")
	public void testAccounts() {
		ArrayResponse<? extends Item> response = this.api.getAccounts().getResponse();
		this.assertArrayNotEmpty(response);
		this.assertItems(response.getItems());
	}
	
	@Test
	@EnabledIf("hostExists")
	public void testTags() {
		ArrayResponse<? extends Item> response = this.api.getTags().getResponse();
		this.assertArrayNotEmpty(response);
		this.assertItems(response.getItems());
	}
	
	@Test
	@EnabledIf("hostExists")
	public void testBudgets() {
		ArrayResponse<? extends Item> response = this.api.getBudgets().getResponse();
		this.assertArrayNotEmpty(response);
		this.assertItems(response.getItems());
	}
	
	@Test
	@EnabledIf("hostExists")
	public void testReminders() {
		ArrayResponse<? extends Item> response = this.api.getReminders().getResponse();
		this.assertArrayNotEmpty(response);
		this.assertItems(response.getItems());
	}
	
	@Test
	@EnabledIf("fullTest")
	public void testGroups() {
		ArrayResponse<? extends Item> response = this.api.getGroups().getResponse();
		this.assertArrayEmpty(response);
	}
	
	@Test
	@EnabledIf("fullTest")
	public void testContacts() {
		ArrayResponse<? extends Item> response = this.api.getContacts().getResponse();
		this.assertArrayEmpty(response);
	}
	
	private void assertOk(BaseResponse response) {
		Assertions.assertNotNull(response);
		Assertions.assertEquals(Status.OK, response.getStatus());
		Assertions.assertNull(response.getError());
	}
	
	private void assertArrayEmpty(ArrayResponse<?> response) {
		this.assertOk(response);
		Assertions.assertNotNull(response.getItems());
		Assertions.assertTrue(response.getItems().isEmpty());
	}
	
	private void assertArrayNotEmpty(ArrayResponse<?> response) {
		this.assertOk(response);
		Assertions.assertNotNull(response.getItems());
		Assertions.assertFalse(response.getItems().isEmpty());
	}
	
	private void assertItem(Item item) {
		Assertions.assertTrue(item.getId() > 0);
	}
	
	private void assertNamedItem(NamedItem item) {
		this.assertItem(item);
		Assertions.assertNotNull(item.getName());
		Assertions.assertNotEquals(0, item.getName().length());
	}
	
	private void assertSpecificItem(Item item) {
		if (item instanceof Transaction) {
			this.assertTransaction((Transaction)item);
		} else if (item instanceof Budget) {
			this.assertBudget((Budget)item);
		} else if (item instanceof Reminder) {
			this.assertReminder((Reminder)item);
		} else if (item instanceof Tag) {
			this.assertTag((Tag)item);
		} else if (item instanceof Account) {
			this.assertAccount((Account)item);
		}
	}
	
	private void assertTransaction(Transaction item) {
		this.assertItem(item);
		Assertions.assertNotNull(item.getType());
		Assertions.assertNotNull(item.getAmount());
		Assertions.assertNotNull(item.getDate());
	}
	
	private void assertAccount(Account item) {
		this.assertNamedItem(item);
		Assertions.assertNotNull(item.getBank());
		Assertions.assertNotEquals(0, item.getBank().length());
		Assertions.assertNotNull(item.getBalance());
	}
	
	private void assertTag(Tag item) {
		this.assertNamedItem(item);
	}
	
	private void assertBudget(Budget item) {
		this.assertNamedItem(item);
	}
	
	private void assertReminder(Reminder item) {
		this.assertNamedItem(item);
		Assertions.assertNotNull(item.getStartDate());
		Assertions.assertTrue(item.getStartDate().isAfter(LocalDate.of(2010, 1, 1)));
	}
	
	private void assertItems(Collection<? extends Item> items) {
		for (Item item : items) {
			this.assertSpecificItem(item);
		}
	}
	
}
