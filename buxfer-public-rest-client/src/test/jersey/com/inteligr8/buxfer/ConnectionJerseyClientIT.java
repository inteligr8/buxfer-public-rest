package com.inteligr8.buxfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.inteligr8.rs.ClientConfiguration;

@TestPropertySource(locations = {"/buxfer-personal.properties"})
@SpringJUnitConfig(classes = {BuxferClientConfiguration.class, BuxferClientJerseyImpl.class})
public class ConnectionJerseyClientIT extends ConnectionClientIT {
	
	@Autowired
	private BuxferClientJerseyImpl client;
	
	@Override
	public BuxferPublicRestApi getClient() {
		return this.client.getApi();
	}
	
	@Override
	public ClientConfiguration getConfiguration() {
		return this.client.getConfig();
	}
	
}
